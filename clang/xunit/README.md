## Instructions

Implement xUnit in C. You may use the cmocka testing framework to guide your work.

Complete these following tasks:

1. Invoke test method
1. Invoke setUp first
1. Invoke teardown afterward
1. Run multiple tests
