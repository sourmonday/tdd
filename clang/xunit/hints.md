## Invoke Test Method Hints

1. Check out how cmocka_unit_tests are defined in allocate_module_test.c
1. _run_test may be deprecated, but it is a good example to check out
1. The pre-processor definition has several elements in it, are you sure you understand every part of it? Ask or Google

## Setup and Teardown Hints

1. Now you have to make sure you can pass state in-between correctly. Do you understand pointers?
1. Take a look at how cmocka does it in _run_test
