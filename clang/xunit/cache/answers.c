#include <stdio.h>
#include <stdlib.h>

typedef int (*UnitTestFunction)(void **state);

typedef int (*FixtureFunction)(void **state);

typedef struct UnitTest {
    const char *name;
    UnitTestFunction test_function;
    FixtureFunction setup_function;
    FixtureFunction teardown_func;
} UnitTest;

#define unit_test(f) { #f, f, NULL, NULL }

#define unit_test_setup_teardown(f, setup, teardown) { #f, f, setup, teardown }

int _run_test(const char * const function_name, const UnitTestFunction Function, FixtureFunction SetUpFunction,
        FixtureFunction TearDownFunction) {

    void *current_state = NULL;
    printf("[ RUN     ] %s\n", function_name);

    if (SetUpFunction)
        SetUpFunction(&current_state);

    int retval = Function(&current_state);
    if (retval)
        printf("[      OK ] %s\n", function_name);
    else
        printf("[  FAILED ] %s\n", function_name);

    if (TearDownFunction)
        TearDownFunction(&current_state);

    return retval;
}

#define run_test(f) _run_test(#f, f, NULL, NULL)

#define run_test_setup(f, setup) _run_test(#f, f, setup, NULL)

#define run_test_teardown(f, teardown) _run_test(#f, f, NULL, teardown)

#define run_test_setup_teardown(f, setup, teardown) _run_test(#f, f, setup, teardown)

int _run_group_test(const char * const group_name, const struct UnitTest * const tests, const size_t num_tests,
        FixtureFunction GroupSetup, FixtureFunction GroupTeardown)
{
    for (unsigned int i = 0; i < num_tests; i++)
    {
        if (GroupSetup && GroupTeardown)
            run_test_setup_teardown(tests[i].test_function, GroupSetup, GroupTeardown);
        else
            run_test(tests[i].test_function);
    }
    return 0;
}

#define run_group_test(tests, setup, teardown) \
    _run_group_test(#tests, tests, sizeof(tests)/sizeof((tests)[0]), setup, teardown)

int testMethod(int reportNum)
{
    return reportNum;
}

static int test_dummy_method(void **state)
{
    if (*state)
        return testMethod(*((int*)*state));
    return testMethod(0);
}

static int test_success_method(void **state)
{
    if (*state)
        return testMethod(*((int*)*state));
    return testMethod(1);
}

static int test_dummy_setup(void **state)
{
    *state = malloc(sizeof(int*));
    int * int_container = *state;
    *int_container = 1;

    return 0;
}

static int test_dummy_teardown(void **state)
{
    if (*state)
        free(*state);
    *state = NULL;
    printf("teardown complete\n");
    return 0;
}

int main() {
    const struct UnitTest tests[] = {
        unit_test(test_dummy_method),
        unit_test(test_success_method)
    };

    return run_group_test(tests, NULL, NULL);
//    return run_test(test_dummy_method);
//    return run_test_setup(test_dummy_method, test_dummy_setup);
//    return run_test_teardown(test_dummy_method, test_dummy_teardown);
//    return run_test_setup_teardown(test_dummy_method, test_dummy_setup, test_dummy_teardown);
}