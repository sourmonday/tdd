//
// Created by Ray Sou on 2018-12-29.
//

#include "money.h"


Currency make_currency(const double new_value, double exchange_rate) {
    Currency ret_ptr = malloc(sizeof(currency));
    if (ret_ptr) {
        ret_ptr->value = new_value;
        ret_ptr->exchange_rate_from_dollars = exchange_rate;
    }

    return ret_ptr;
}

double currency_add(Currency obj1, Currency obj2) {
    if (!obj1 || !obj2)
        return -1;

    double obj1_in_usd = obj1->value / obj1->exchange_rate_from_dollars;
    double obj2_in_usd = obj1->value / obj2->exchange_rate_from_dollars;

    return obj1_in_usd + obj2_in_usd;
}

void currency_multiplication(Currency obj, const double multiplier) {
    if (obj)
        obj->value *= multiplier;
}

void currency_destroy(Currency obj) {
    free_if_not_null(obj);
}

bool currency_compare(Currency obj1, Currency obj2) {
    if (obj1 && obj2)
        return (obj1->value == obj2->value) && (obj1->exchange_rate_from_dollars == obj2->exchange_rate_from_dollars);
    return false;
}

bool object_compare(Currency obj1, Currency obj2) {
    if (!obj1 || !obj2 || !currency_compare(obj1, obj2))
        return false;
    return obj1 == obj2;
}


void dollar_static_multiply(Dollar obj, const double multiplier, Dollar *pp_output_obj) {
    *pp_output_obj = make_dollar(obj->value);

    dollar_multiplication(*pp_output_obj, multiplier);
}


void franc_static_multiply(Franc obj, const double multiplier, Franc *pp_output_obj) {
    *pp_output_obj = make_franc(obj->value);

    franc_multiplication(*pp_output_obj, multiplier);
}
