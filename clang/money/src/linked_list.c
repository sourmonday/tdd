//
// Created by Ray Sou on 2018-12-31.
//

#include <memory.h>
#include "linked_list.h"

#include "std_include.h"

int list_init(plist_t *list_pp) {
    if (*list_pp)
        return success_code;

    *list_pp = (plist_t)malloc(sizeof(list_t));
    memset(*list_pp, 0, sizeof(list_t));
    return success_code;
}


int list_insert(plist_t list_p, void *data) {
    if (!list_p)
        return fail_code;

    void *data_copy = malloc(sizeof(void *));
    data_copy = data;
    if (!list_p->head) {
        list_p->head = malloc(sizeof(node_t));
        list_p->head->data = data_copy;

        list_p->head->data = data;


//        *(list_head_data(list_p)) = *data;
        list_p->head->next = NULL;
        list_p->tail = list_p->head;
    }

    list_p->list_size += 1;

    return success_code;
}
