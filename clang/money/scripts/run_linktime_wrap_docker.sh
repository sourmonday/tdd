#!/usr/bin/env bash

docker run -i -v $PWD:/test memory-test:0.2 bash -c "mkdir -p /docker; cp -r /test/* /docker; \
cd /docker; cmake . ; make ; cd test/linked_list ; ./test_linked_list_malloc_calls"
