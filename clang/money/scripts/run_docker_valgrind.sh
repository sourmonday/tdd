#!/usr/bin/env bash

# First arg is used as a path

docker run -i -v $1:/test memory-test:0.2 bash -c "mkdir -p /docker; cp -r /test/* /docker; \
cd /docker; cmake . && make && valgrind --leak-check=full --leak-resolution=med --show-leak-kinds=all \
--track-origins=yes --vgdb=no --tool=memcheck -v --gen-suppressions=all ./test/test_money"
