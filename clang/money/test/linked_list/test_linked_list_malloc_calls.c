
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include "linked_list.h"

#include <printf.h>

/*
 * TODO: Fix the test wrapping for OSX
 */


static void test_list_created(void **state) {
    (void) state;  /* unused */
    int rv = 0;

    list_t *new_list = NULL;
    rv = list_init(&new_list);

    assert_non_null(new_list);
    assert_int_equal(rv, 0);
}

static void test_just_return_if_list_passed_for_creation(void **state) {
    (void) state;  /* unused */
    int rv = 0;

    list_t *new_list = (plist_t)0x80000000;
    list_t *expected_value = (plist_t)new_list;

    rv = list_init(&new_list);
    assert_ptr_equal(new_list, expected_value);
    assert_int_equal(rv, 0);
}

static void test_insert_null_list_fails(void **state) {
    (void) state;  /* unused */
    int rv = 0;
    int data = 0;

    list_t *new_list = NULL;
    rv = list_insert(new_list, (void *)&data);
    assert_int_equal(rv, fail_code);
}



int main(void) {
    const struct CMUnitTest tests[] = {
            cmocka_unit_test(test_list_created),
            cmocka_unit_test(test_just_return_if_list_passed_for_creation),
            cmocka_unit_test(test_insert_null_list_fails),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}
