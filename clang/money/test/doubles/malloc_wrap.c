//
// Created by Ray Sou on 2018-12-31.
//

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include "malloc_wrap.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

static int called_count = 0;

void *__wrap_malloc(size_t size) {
    (void)size;

    printf("[   MOCK   ] in __wrap_malloc call: %d count\n", called_count);
    called_count += 1;
    printf("[   MOCK   ] in __wrap_malloc call: %d count\n", called_count);

    return (void *)0x80000000;
}

void __wrap_free(void *ptr) {
    if (!ptr)
    {
        printf("[MOCK ERROR] free called on NULL ptr!!!!!!!!!!!!!!!!!!!");
        return;
    }

    printf("[   MOCK   ] in __wrap_free call: %d count\n", called_count);
    called_count -= 1;
    printf("[   MOCK   ] in __wrap_free call: %d count\n", called_count);
    if (called_count <= 0)
        ptr = NULL;
}

int num_wrapped_mallocs() {
    return called_count;
}
