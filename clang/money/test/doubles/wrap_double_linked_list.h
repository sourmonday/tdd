//
// Created by Ray Sou on 2018-12-31.
//

#ifndef PROJECT_WRAP_DOUBLE_LINKED_LIST_H
#define PROJECT_WRAP_DOUBLE_LINKED_LIST_H

#include "linked_list.h"

int __wrap_list_init(plist_t *list_pp);

#endif //PROJECT_WRAP_DOUBLE_LINKED_LIST_H
