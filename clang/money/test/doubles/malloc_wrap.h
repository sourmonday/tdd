//
// Created by Ray Sou on 2018-12-31.
//

#include <stdlib.h>


#ifdef UNIT_TESTING

void *__wrap_malloc(size_t size);

void __wrap_free(void *ptr);

int num_wrapped_mallocs();

#endif
