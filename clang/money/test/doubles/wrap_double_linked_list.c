//
// Created by Ray Sou on 2018-12-31.
//

#include "wrap_double_linked_list.h"

#include <stdio.h>

extern int __real_list_init(plist_t *list_pp);

static list_t test_list = { NULL, NULL, 0 };

int __wrap_list_init(plist_t *list_pp) {
    printf("[   MOCK   ] in __wrap_list_init call\n");
    if (*list_pp)
        return __real_list_init(list_pp);

    printf("[   MOCK   ] __wrap_list_init: NULL passed, returning list of size %d\n", (int)test_list.list_size);
    *list_pp = &test_list;

    return success_code;
}
