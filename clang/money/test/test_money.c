
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdlib.h>

#include "money.h"


static void test_dummy_always_succeeds(void **state) {
    (void) state;  /* unused */

    assert_int_equal(3, 3);
}

static void test_multiplication(void **state) {
    (void) state;  /* unused */

    Dollar five = make_dollar(5);
    dollar_multiplication(five, 2);
    assert_in_range(dollar_value(five), 10-0.01, 10+0.01);

    dollar_multiplication(five, 3);
    assert_in_range(dollar_value(five), 30-0.01, 30+0.01);

    free_if_not_null(five);
}

static void test_static_multiplication(void **state) {
    (void) state;  /* unused */

    Dollar five = make_dollar(5);
    Dollar ten = NULL;

    dollar_static_multiply(five, 2, &ten);

    assert_in_range(dollar_value(five), 5-0.01, 5+0.01);
    assert_in_range(dollar_value(ten), 10-0.01, 10+0.01);

    free_if_not_null(five);
    free_if_not_null(ten);
}

static void test_equality(void **state) {
    (void) state;  /* unused */

    Dollar five1 = make_dollar(5);
    Dollar five2 = make_dollar(5);
    Dollar ten = make_dollar(10);
    assert_true(dollar_compare(five1, five2));
    assert_memory_equal(five1, five2, sizeof(dollar));

    Franc five_francs = make_franc(5);
    assert_false(currency_compare(five1, five_francs));
    assert_memory_not_equal(five1, five_francs, sizeof(currency));

    assert_false(object_compare(five1, ten));
    assert_false(object_compare(five1, five2));
    assert_true(object_compare(five1, five1));

    free_if_not_null(five1);
    free_if_not_null(five2);
    free_if_not_null(five_francs);
    free_if_not_null(ten);
}

static void test_franc_multiplication(void **state) {
    (void) state;  /* unused */

    Franc five = make_franc(5);

    franc_multiplication(five, 2);
    assert_in_range(franc_value(five), 10-0.01, 10+0.01);

    free_if_not_null(five);
}

static void test_franc_static_multiplication(void **state) {
    (void) state;  /* unused */

    Franc five = make_franc(5);
    Franc ten = NULL;

    franc_static_multiply(five, 2, &ten);

    assert_in_range(franc_value(five), 5-0.01, 5+0.01);
    assert_in_range(franc_value(ten), 10-0.01, 10+0.01);

    free_if_not_null(five);
    free_if_not_null(ten);
}

static void test_add_different_currencies(void **state) {
    (void) state;

    Franc ten_francs = make_franc(10);
    Dollar ten_dollars = make_dollar(10);

    double total_usd = currency_add(ten_dollars, ten_francs);
    double expected = ten_dollars->value + ten_francs->value / FRANC_EXCH_RT;

    assert_in_range(total_usd, expected - 0.01, expected + 0.01);

    free_if_not_null(ten_dollars);
    free_if_not_null(ten_francs);
}

int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_dummy_always_succeeds),
        cmocka_unit_test(test_multiplication),
        cmocka_unit_test(test_equality),
        cmocka_unit_test(test_static_multiplication),
        cmocka_unit_test(test_franc_multiplication),
        cmocka_unit_test(test_franc_static_multiplication),
        cmocka_unit_test(test_add_different_currencies),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}
