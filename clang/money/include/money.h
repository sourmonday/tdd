
#ifndef MONEY_MONEY_H
#define MONEY_MONEY_H

#include "std_include.h"

#define DOLLAR_EXCH_RT 1.0
#define FRANC_EXCH_RT 0.98


typedef struct _currency {
    double value;
    double exchange_rate_from_dollars;
} currency, *Currency;

Currency make_currency(double new_value, double exchange_rate);

void currency_destroy(Currency obj);

/*
 * Returns the value in dollars
 */
double currency_add(Currency obj1, Currency obj2);

void currency_multiplication(Currency obj, double multiplier);

/*
 * Compares the values in the Currency types. If either object is NULL, returns false
 */
bool currency_compare(Currency obj1, Currency obj2);

bool object_compare(Currency obj1, Currency obj2);

#define dollar currency
#define Dollar Currency
#define make_dollar(v) make_currency(v, DOLLAR_EXCH_RT)
#define dollar_multiplication currency_multiplication
#define dollar_value(d) ((d)->value)
#define dollar_compare currency_compare


void dollar_static_multiply(Dollar obj, double multiplier, Dollar *pp_output_obj);


#define franc currency
#define Franc Currency
#define make_franc(v) make_currency(v, FRANC_EXCH_RT)
#define franc_multiplication currency_multiplication
#define franc_value dollar_value
#define franc_compare currency_compare


void franc_static_multiply(Franc obj, double multiplier, Franc *pp_output_obj);

#endif //MONEY_MONEY_H
