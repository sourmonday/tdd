//
// Created by Ray Sou on 2018-12-31.
//

#ifndef PROJECT_STD_INCLUDE_H
#define PROJECT_STD_INCLUDE_H

#include <stdbool.h>
#include <stdlib.h>

#ifdef UNIT_TESTING
extern void* _test_malloc(const size_t size, const char* file, const int line);
extern void* _test_calloc(const size_t number_of_elements, const size_t size,
                          const char* file, const int line);
extern void _test_free(void* const ptr, const char* file, const int line);

#define malloc(size) _test_malloc(size, __FILE__, __LINE__)
#define calloc(num, size) _test_calloc(num, size, __FILE__, __LINE__)
#define free(ptr) _test_free(ptr, __FILE__, __LINE__)
#endif // UNIT_TESTING

#define free_if_not_null(p) if (p) free(p)

#endif //PROJECT_STD_INCLUDE_H
