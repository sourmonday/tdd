//
// Created by Ray Sou on 2018-12-31.
//

#ifndef PROJECT_LINKED_LIST_H
#define PROJECT_LINKED_LIST_H

#include "std_include.h"

#define success_code 0
#define fail_code -1

typedef struct node_t_ {
    void *data;
    struct node_t_ *next;
} node_t, *pnode_t;

typedef struct list_t_ {
    pnode_t head;
    pnode_t tail;
    size_t list_size;
} list_t, *plist_t;

int list_init(plist_t *list_pp);

// inserts a shallow COPY of the data passed
int list_insert(plist_t list_p, void *data);

void list_destroy(plist_t *list_pp);

#define list_size(list) ((list)->list_size)

#define list_head(list) ((list)->head)
#define list_tail(list) ((list)->tail)

#define list_head_data(list) (list_head((list))->data)
#define list_tail_data(list) (list_tail((list))->data)

#define deref_data_to_int(data_pointer) (*(int *)(data_pointer))

#endif //PROJECT_LINKED_LIST_H
