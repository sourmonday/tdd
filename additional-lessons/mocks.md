## Introduction

According to Wikipedia, mock objects are simulated objects that mimic the behavior of real objects in controlled ways.

Mock objects are pre-programmed with expectations of the calls they should expect to receive. They might throw
exceptions if un-expected calls are received. You do not mock the system under test. You use mock objects to stand in
for the real neighbors of the object under test while the tests run.

Mocks should not be written for code that you do not control, that is, third-party code. In these cases, write an
adapter layer. There are some exceptions to this -- you might use mocks to simulate behavior that is hard to trigger
with the real library, such as throwing exceptions.

![Mock Object from xunitpatterns](/img/mock_object.gif)

## When To Use It

You should not use a Mock Object when a failed assertion cannot be reported effectively back to the Test Runner. In
these cases, you should probably use a [Test Spy](spies.md) instead.

## Example

TODO

## Documentation

1. [<i>Growing Object-Oriented Software, Guided by Tests</i>](https://www.amazon.com/dp/0321503627/ref=cm_sw_r_tw_dp_U_x_XrRjCb547GSA3) -- Steve Freeman & Nat Pryce
1. https://en.wikipedia.org/wiki/Mock_object
1. https://blog.pragmatists.com/test-doubles-fakes-mocks-and-stubs-1a7491dfa3da
1. http://xunitpatterns.com/Mock%20Object.html


https://lwn.net/Articles/558106/