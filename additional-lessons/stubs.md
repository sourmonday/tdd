## Introduction

According to Wikipedia, test stubs are programs that simulate behaviors of software components for system under tests.
They provide canned answers to calls made during the test, usually not responding to anything outside of what was
programmed.

The test stubs are configured to give only the appropriate values so that the system under test will properly exercise
untested code.

![Stub from xunitpatterns](/img/test_stub.gif)

## When To Use It

You use a stub when you do not have the ability to control the indirect inputs but do not need to verify the indirect
outputs. If you need an observation point in order to verify the indirect outputs, consider a [mock object](mocks.md)
 or a [test spy](spies.md) instead.

## Example

TODO

## Documentation

1. https://blog.pragmatists.com/test-doubles-fakes-mocks-and-stubs-1a7491dfa3da
1. http://xunitpatterns.com/Test%20Stub.html
1. https://en.wikipedia.org/wiki/Test_stub
