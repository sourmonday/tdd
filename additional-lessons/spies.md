## Introduction

Test spies are essentially stubs that also implement an observation point that exposes the indirect outputs of the 
system under test.

## When To Use It

You use a Test Spy when you need to observe the side-effects of invoking methods on the system under test. You should
use a Test Spy when any of hte following are true:

* You are verifying the indirect outputs of the system under test and you <b>cannot</b> predict the value of all 
attributes of the interactions ahead of time
* You want the assertions to be visible in the test and Mock Object expectations are not sufficiently intent-revealing
* Your test requires test-specific equality. Pre-defined equalities will not cut it and you are using tools that 
generate the mock object but do not give you control over the assertion methods
* A failed assertion cannot be reported back effectively to the Test Runner

## Example

TODO

## Documentation

1. http://xunitpatterns.com/Test%20Spy.html
