## Introduction

Fake objects are working implementations of neighbors to the system under test that has some kind of simplification
that wouldn't work well in a production environment. The canonical example is using an in-memory database using a
HashMap as opposed to using an actual database.

Real objects are often designed while balancing many engineering choices, such as scalability. A Fake does not need to
have any of that. In the cases where such Fake objects cannot be acquired, we may build this lightweight version of the
depended-on component.

![Fake Object from xunitpatterns](/img/fake_object.gif)

In order to use a Fake Object, you are primarly concerned with two objectives:

* Building the Fake Object Implementation, and
* Installing the Fake Object

Because you might have to build the Fake to replace the real object, you might have it only support a specific subset of
tests to validate its accurate representation of the limited functionality. Afterwards, you will need to inject your
dependency into the appropriate object.

## When To Use It

You choose to make a Fake when the tests need more complex sequences of behavior than is worth implementing in a
[Test Stub](stubs.md) or a [Mock Object](mocks.md). It must also be easier to create the lightweight implementation 
than it would be to build a suitable Mock Object (in the long run).

If you need to control either indirect inputs or indirect outputs of the system under test, you should probably be using
a Mock Object or Test Stub instead.

## Example

TODO

## Documentation

1. https://en.wikipedia.org/wiki/Test_double
1. https://blog.pragmatists.com/test-doubles-fakes-mocks-and-stubs-1a7491dfa3da
1. http://xunitpatterns.com/Fake%20Object.html
