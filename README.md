# Repository Details

This will be based on Kent Beck's TDD, by Example and Martin Fowler's Refactoring books. This repository is used to 
learn programming langauges using TDD.

---

# Introduction

Test Driven Development commits to two primary rules:

* We only write new code when an automated test has failed
* We eliminate duplication

While these are simple rules, several things become implied by following them. Design should happen organically, 
automated tests must be written by the developer, the development environment must provide feedback on all changes 
(to include the fact that previous features have not been broken), and so on.

Here are the implied order to the tasks of programming:

1. <b>Red</b> -- Write a little test that doesn't work, and perhaps doesn't even compile at first.
1. <b>Green</b> -- Make the test work quickly, committing whatever sins necessary in the process.
1. <b>Refactor</b> -- Eliminate all of hte duplication created in merely getting hte test to work.

That is the mantra of TDD: Red/Green/Refactor.

---

#. Organization of the Sections

1. Live Demonstration with Open Source Code Base
1. Mechanics of a Unit Testing Framework
1. TDD, By Example's Money Example
1. Student Lab: xUnit Example with C
1. Refactoring
1. Student Lab: Using Automated Tests to Refactor Legacy Code
1. Patterns for Test-Driven Development, to include Fakes, Mocks and Stubs
1. Student Lab: Using Automated Tests to Integrate Third Party Code

Bonus Labs:

Continue to build out the above with increased injections of "legacy" code, and third party libraries to build
out the Money Example to incorporate a database, play in a larger environment of a bank or some kind of exchange 
service, etc.

---

## Live Demonstration Notes

Select popular, beginner-friendly project in the target language from:

https://github.com/MunGell/awesome-for-beginners

For the purposes of this, we will select Python's [MITMProxy Project](https://github.com/mitmproxy/mitmproxy)

One thing to note is that the project provides an easy script to setup the development environment. This is a key to 
ensuring reproducible results across team members because it minimizes "Well, it works on my machine" issues.

Run the tests by issuing the commands:

```bash
. venv/bin/activate  # activate the virtual environment in Python
tox
```

In group discussion, talk about different parts of the test report, the results, etc. How does the repository ensure 
the fidelity of commits coming into the project? Where do tests fall in that process?

---

## Mechanics of a Unit Test

Ensure you are testing behaviors, not methods. Testing the method //TODO// doesn't necessarily guarantee a code base
that is easy to work with. While testing methods might give high code coverage, the developer's concern is guaranteeing
<i>features</i> rather than individual methods.

Your tests should be isolated. They should not affect one another.

#### Phases: Setup, Exercise, Verify, Teardown

The full read of xUnit basics is found [here](http://xunitpatterns.com/XUnitBasics.html).

1. <b>Setup</b>: set up your test fixture
1. <b>Exercise</b>: exercise the system under test by interacting with the methods in the API
1. <b>Verify</b>: verify the expected outcome
1. <b>Teardown</b>: tear down the fixture

![xunitpatterns static test structure](img/test_structure.gif)

The general TDD Cycle Goes:

1. Write a Test: Invent the interface you wish you had and include all elements you imagine will be necessary to 
make it work
1. Make it run: Get the test to run green, regardless of any and all sins (aesthetics and smells)
1. Make it right: Now that the system works as intended, you're not done! Fix the engineering sins now!

### Strategies for Getting to Green

1. Fake it -- return a constant and gradually replace constants with variables until you have the real code
1. Use Obvious Implementation: just create what you think is the instinctual "right code" and test it. You might need
to occasionally back up and fake parts when you get a red bar.
1. Triangulation: create multiple points (2 or more) where some evaluate to true and others to false and cut in 
between those cases to find the general solution

---

## TDD, By Example's Money Example (bookmarks)

Do: Chapter 1-7, 15

Note for C: You will need to deal with the lack of object-oriented facilities in C.

---

## STUDENT LAB: xUnit Example

Here's the to-do list that comes to mind when creating a testing framework.

1. Invoke test method
1. Invoke setUp first
1. Invoke tearDown afterward
1. Run multiple tests

Note for C: Use cmocka as a reference to assist in building out for C. <br>
Note for C++: Use the TDD, By Example listing for Python and just invoke in C++ <br>
Note for Python: TDD, By Example already lists a good example

If you have more time, clean up your implementation. Implement more features of cmocka and try to understand it.

---

## Refactoring: What and When

### What

Code refactoring is the process of restructuring existing code, i.e., the "factoring," without changing its behavior.
It is intended to improve nonfunctional attributes, such as readability and complexity. Coding typically happens by 
achieving short term goals, often without full knowledge of the entire scope of the architecture. Re-factoring helps 
communicate information to the developers more quickly.

### When

Martin Fowler mentions the Rule of Three given to him by Don Roberts:

"The first time you do something, you just do it. The second time you do something similar, you wince at the 
duplication, but you do the duplicate thing anyway. The third time you do something similar, you refactor."

You might also refactor in the following circumstances:

1. Preparatory:  Making it Easier to Add a Feature
1. Comprehension Refactoring: Making Code Easier to Understand
1. Litter Pickup: Code is a Little Smelly

###### When Not To

If you run across code that is a mess, but you don't need to modify it, then don't refactor it. Some ugly code that 
is behind an API may remain ugly. It's only when you need to understand the code that refactoring gives any benefit. 
Ensure that you are gaining something for putting in the effort of refactoring. Don't refactor for refactoring's sake.

---

## STUDENT LAB: Refactoring Legacy Code

Take a crack at something from https://github.com/legacycoderocks/awesome-legacy-code#c

---

## Patterns for Test-Driven Development

### Test Doubles

#### Terms

1. Test Double: a light implementation of a component of the system under test to support testing when testing with the
real object is infeasible. Test Doubles represent a production object in a testing environment by exposing the same
interface
1. Dummy Object: objects that are passed around but never actually used. Usually used to fill parameter lists
1. [Stub](additional-lessons/stubs.md): provide canned answers to calls made
1. [Spy](additional-lessons/spies.md): a stub that also records some information based on how they were called
1. [Mock Object](additional-lessons/mocks.md): pre-programmed with expectations of the calls they should expect to 
receive. Might throw exceptions if un-expected calls are received
1. [Fake Object](additional-lessons/fakes.md): objects that have working implementations, but take some kind of a 
shortcut that makes it non-scalable
1. [indirect input](http://xunitpatterns.com/indirect%20input.html): when the system under test is affected by values
returned by another component
1. [indirect output](http://xunitpatterns.com/indirect%20output.html): when the behavior of the system under test
includes side effects or other method calls that cannot be observed through the public API

(see examples primarily in CMocka's example directory. WIP: completed money examples, as well)

#### Reasons for Use

If an object has any of the following characteristics, it may be useful to use a test double in its place:

* the object supplies non-deterministic results (e.g., current time or current temperature)
* it has states that are difficult to create or reproduce
* it is slow
* it does not yet exist or may change behavior
* it would have to include information and methods exclusively for testing purposes (and not for its actual task)

---

#### Resources

1. https://blog.pragmatists.com/test-doubles-fakes-mocks-and-stubs-1a7491dfa3da
1. http://xunitpatterns.com/Test%20Double.html
1. https://martinfowler.com/articles/mocksArentStubs.html
1. https://stackoverflow.com/questions/346372/whats-the-difference-between-faking-mocking-and-stubbing
1. https://en.wikipedia.org/wiki/Mock_object

---

## STUDENT LAB: Third Party Code Integration

[GitHub Trending C Projects](https://github.com/trending/c)

Find a repository of interest and integrate it into your money lab! You may consult with the instructor in order to 
strategize on what and how to integrate it but you may not consult with classmates.

You may ask for help on setup of your development environment from anyone if additional set-up is warranted.
You may help each other with defining and conceptualizing the test doubles as you use them.

---

## Additional Reading

1. Valgrind Code Coverage: https://benjamin-meyer.blogspot.com/2007/12/valgrind-callgrind-tools-part-3-code.html
1. Using Valgrind to Profile Your Application for Performance: https://accu.org/index.php/journals/1886
1. Using another testing framework: Google Test, Boost.Test, Catch --- https://www.jetbrains.com/clion/features/unit-testing.html
1. DTrace: Use as a performance analysis and troubleshooting tool -- see [Reference Manual](http://dtrace.org/guide/preface.html#preface)
and [recipe book](https://github.com/opendtrace/toolkit)
1. Gradle: Build automation system 
